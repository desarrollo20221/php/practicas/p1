<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2 de la hoja de Ejercicio 1 de PHP</title>
    </head>
    <body>
        <table width="100%" border="1">
            <tr>
            <td>
                <?php
                    // Cuando utiliceis la instruccion echo puedes utilizar la comilla dobles o simples
                    echo 'Este texto quiero que lo escribas utilizando la funcion echo de php';
                ?> 
            </td>
            <td>Aqui debe de colocar un texto directamente en HTML</td>
            </tr>
            <tr>
                <td>
                    <?php
                    // Cuando utiliceis la instruccion print puedes utilizar la comilla dobles o simples
                    print 'Este texto quiero que lo escribas utilizando la funcion print de php';
                    ?> 
                </td>
                <td>
                    <?php
                    echo 'Academia Alpe';
                    ?>
                </td>
            </tr>
        </table>
        
    </body>
</html>
